package com.nextlabs.io.testapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import nextlabs.io.nextinstalltracker.TrackInstalls;

public class InstallReferralReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        TrackInstalls tr = new TrackInstalls(context);
        tr.informServer();
    }
}
